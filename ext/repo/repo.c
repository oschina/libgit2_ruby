#include "repo.h"
#include "stdio.h"
#include "stdlib.h"

const char *REPO_ERROR_NAMES[] = {
	"None",            /* GITERR_NONE */
	"NoMemError",      /* GITERR_NOMEMORY, */
	"OSError",         /* GITERR_OS, */
	"InvalidError",    /* GITERR_INVALID, */
	"ReferenceError",  /* GITERR_REFERENCE, */
	"ZlibError",       /* GITERR_ZLIB, */
	"RepositoryError", /* GITERR_REPOSITORY, */
	"ConfigError",     /* GITERR_CONFIG, */
	"RegexError",      /* GITERR_REGEX, */
	"OdbError",        /* GITERR_ODB, */
	"IndexError",      /* GITERR_INDEX, */
	"ObjectError",     /* GITERR_OBJECT, */
	"NetworkError",    /* GITERR_NET, */
	"TagError",        /* GITERR_TAG, */
	"TreeError",       /* GITERR_TREE, */
	"IndexerError",    /* GITERR_INDEXER, */
	"SslError",        /* GITERR_SSL, */
	"SubmoduleError",  /* GITERR_SUBMODULE, */
	"ThreadError",     /* GITERR_THREAD, */
	"StashError",      /* GITERR_STASH, */
	"CheckoutError",   /* GITERR_CHECKOUT, */
	"FetchheadError",  /* GITERR_FETCHHEAD, */
	"MergeError",      /* GITERR_MERGE, */
	"SshError",        /* GITERR_SSH, */
	"FilterError"      /* GITERR_FILTER, */
};


#define REPO_ERROR_COUNT (int)((sizeof(REPO_ERROR_NAMES)/sizeof(REPO_ERROR_NAMES[0])))

VALUE rb_eRepoErrors[REPO_ERROR_COUNT];

inline void repo_exception_raise(void) {
	VALUE err_klass, err_obj;
	const git_error *error;
	const char *err_message;

	error = giterr_last();

	if (error && error->klass > 0 && error->klass < REPO_ERROR_COUNT) {
		err_klass = rb_eRepoErrors[error->klass];
		err_message = error->message;
	} else {
		err_klass = rb_eRuntimeError;
		err_message = "Repo operation failed";
	}

	err_obj = rb_exc_new2(err_klass, err_message);
	giterr_clear();
	rb_exc_raise(err_obj);
}

inline void repo_exception_check(int errorcode)
{
	if (errorcode < 0)
		repo_exception_raise();
}

/*
 *  call-seq:
 *     Repo.version -> version
 *
 *  Returns an array representing the current libgit2 version in use. Using
 *  the array makes it easier for the end-user to take conditional actions
 *  based on each respective version attribute: major, minor, rev.
 *
 *    Repo.version #=> [0, 17, 0]
 */
VALUE rb_repo_libgit2_version(VALUE self)
{
	int major;
	int minor;
	int rev;

	git_libgit2_version(&major, &minor, &rev);

	// We return an array of three elements to represent the version components
	return rb_ary_new3(3, INT2NUM(major), INT2NUM(minor), INT2NUM(rev));
}

/*
* Repo.init_at('repository', :bare) #=> #<repo::Repository:0x108849488>
*/
VALUE rb_repo_init_at(VALUE self, VALUE path, VALUE is_bare) {
	git_repository *repo = NULL;
	int error;

	error =	git_repository_init(&repo, StringValueCStr(path), RTEST(is_bare));
	repo_exception_check(error);
	git_repository_free(repo);

	return Qtrue;
}

VALUE rb_repo_discover(VALUE self, VALUE path) {
	git_buf repository_path = { NULL };
	int error;

	Check_Type(path, T_STRING);

	error = git_repository_discover(
		&repository_path,
		StringValueCStr(path),
		0,
		NULL
	);
	repo_exception_check(error);

	return rb_str_new((const char*)repository_path.ptr, repository_path.size);
}

VALUE rb_repo_do_clone(VALUE self, VALUE path, VALUE url) {
	git_clone_options options = GIT_CLONE_OPTIONS_INIT;
	git_repository *repo;
	int error;

	options.bare = GIT_REPOSITORY_INIT_BARE;

	Check_Type(path, T_STRING);
	Check_Type(url, T_STRING);

	error = git_clone(&repo, StringValueCStr(url), StringValueCStr(path), &options);
	repo_exception_check(error);
	git_repository_free(repo);

	return Qtrue;
}

VALUE rb_repo_do_fetch(VALUE self, VALUE path, VALUE url) {
	git_remote *remote;
	git_repository *repo;
	int error;
	char *log_message = NULL;
	git_fetch_options opts = GIT_FETCH_OPTIONS_INIT;

	char *fetch_refspec_strings[] = {
		"+refs/heads/*:/refs/heads/*",
		"+refs/tags/*:/refs/tags/*"
	};
	git_strarray refspecs = {fetch_refspec_strings, 2};

	Check_Type(url, T_STRING);

	error = git_remote_create_anonymous(
			&remote,
			repo,
			StringValueCStr(url));
	repo_exception_check(error);

	error = git_remote_fetch(remote, &refspecs, &opts, log_message);
	repo_exception_check(error);
	git_repository_free(repo);
	git_remote_free(remote);

	return Qtrue;
}

void Init_repo(void) {
	int i;
	VALUE rb_cRepo;
	VALUE rb_cModule;
	VALUE rb_eRepoError;

	rb_cModule = rb_define_module("Repo", rb_cObject);
	rb_eRepoError = rb_define_class_under(rb_cModule, "Error", rb_eStandardError);
	rb_eRepoErrors[0] = Qnil; /* 0 return value -- no exception */
	rb_eRepoErrors[1] = rb_define_class_under(rb_cModule, REPO_ERROR_NAMES[1], rb_eNoMemError);
	rb_eRepoErrors[2] = rb_define_class_under(rb_cModule, REPO_ERROR_NAMES[2], rb_eIOError);
	rb_eRepoErrors[3] = rb_define_class_under(rb_cModule, REPO_ERROR_NAMES[3], rb_eArgError);

	for (i = 4; i < REPO_ERROR_COUNT; ++i) {
		rb_eRepoErrors[i] = rb_define_class_under(rb_cModule, REPO_ERROR_NAMES[i], rb_eRepoError);
	}

	rb_cRepo = rb_define_class_under(rb_cModule, "Repo", rb_cObject);
	rb_define_singleton_method(rb_cRepo, "libgit2_version", rb_repo_libgit2_version, 0);
	rb_define_method(rb_cRepo, "init_at", rb_repo_init_at, 2);
	rb_define_method(rb_cRepo, "discover", rb_repo_discover, 1);
	rb_define_method(rb_cRepo, "do_clone", rb_repo_do_clone, 2);
	rb_define_method(rb_cRepo, "do_fetch", rb_repo_do_fetch, 2);
}