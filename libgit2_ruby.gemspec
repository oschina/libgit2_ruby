$:.push File.expand_path("../lib", __FILE__)

Gem::Specification.new do |s|
  s.name                  = "libgit2_ruby"
  s.version               = "0.0.1"
  s.date                  = Time.now.strftime('%Y-%m-%d')
  s.summary               = "libgit2_ruby is a Ruby binding to the libgit2 linkable library"
  s.homepage              = "https://git.oschina.net/oschina/libgit2_ruby"
  s.email                 = "zouqilin@csu.edu.cn"
  s.authors               = [ "zouqilin", "Charlie" ]
  s.license               = "MIT"
  s.files                 = %w( README.md LICENSE )
  s.files                 += Dir.glob("lib/**/*.rb")
  s.files                 += Dir.glob("ext/**/*.[ch]")
  s.files                 += Dir.glob("vendor/libgit2/cmake/**/*")
  s.files                 += Dir.glob("vendor/libgit2/{include,src,deps}/**/*")
  s.files                 += Dir.glob("vendor/libgit2/{CMakeLists.txt,Makefile.embed,AUTHORS,COPYING,libgit2.pc.in}")
  s.extensions            = ['ext/repo/extconf.rb']
  s.required_ruby_version = '>= 1.9.3'
  s.description           = <<desc
libgit2_ruby is a Ruby bindings to the libgit2 linkable C Git library. This is
for testing and using the libgit2 library in a language that is awesome.
desc
  s.add_development_dependency "rake-compiler", ">= 0.9.0"
  s.add_development_dependency "pry"
  s.add_development_dependency "minitest", "~> 3.0"
end
